from django.shortcuts import render
from todos.models import TodoList
# Create your views here.


def todo_list_list(request):
    items_todo = TodoList.objects.all()
    context = {
        "todo_list": items_todo
    }
    return render(request, "todos/list.html", context)
